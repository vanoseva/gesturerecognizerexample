#include "HelloWorldScene.h"
#include "Utils.h"


USING_NS_CC;


Scene* HelloWorld::createScene()
{
    auto scene = Scene::create();
    auto layer = HelloWorld::create();
    scene->addChild(layer);

    return scene;
}

HelloWorld::HelloWorld() : Layer()
{
    _gestureRecognizer = NULL;
    _sprite = NULL;
    _label = NULL;
}

HelloWorld::~HelloWorld()
{
    CC_SAFE_RELEASE(_gestureRecognizer);
    CC_SAFE_RELEASE(_sprite);
    CC_SAFE_RELEASE(_label);
}

bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    // Init gesture recognizer
    initGestureRecognizer();
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    // Init info label
    auto label = LabelTTF::create("Make any gesture...", "Arial", 40);
    label->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height * 0.1f));
    addChild(label, 1);
    
    _label = label;
    _label->retain();

    // Init sprite
    auto sprite = Sprite::create("HelloWorld.png");
    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    addChild(sprite, 0);
    
    _sprite = sprite;
    _sprite->retain();
    
    return true;
}

void HelloWorld::initGestureRecognizer()
{
    // Init gesture recognizer
    _gestureRecognizer = new TSimpleGestureRecognizer();
    _gestureRecognizer->init();
    _gestureRecognizer->setGestureHandler(this);
    
    // Enable all gesture kinds
    _gestureRecognizer->setTapEnabled(true);
    _gestureRecognizer->setDoubleTapEnabled(true);
    _gestureRecognizer->setLongPressEnabled(true);
    _gestureRecognizer->setPanEnabled(true);
    _gestureRecognizer->setPinchEnabled(true);
    _gestureRecognizer->setRotationEnabled(true);
    _gestureRecognizer->setSwipeEnabled(true);
    
    // Taps will be fired immediately without waiting for double tap
    _gestureRecognizer->setTapRequiresDoubleTapRecognitionToFail(false);
    
    // Other config
    // _gestureRecognizer->setTapThreshold(1.0f);
    // _gestureRecognizer->setLongPressThreshold(1.0f);
    // _gestureRecognizer->setDoubleTapInterval(0.3f);
    // _gestureRecognizer->setPinchFingersDistanceChangeTolerance(0.1f);
    // _gestureRecognizer->setRotationFingersDistanceChangeTolerance(0.5f);
    // _gestureRecognizer->setSwipeThreshold(0.3f);
    
    //
    // IMPORTANT:
    // For multiple touch gestures on iOS (pinch, rotation), always remember tu put
    // the below line of code right after creating the CCEAGLView in AppController.mm
    // [eaglView setMultipleTouchEnabled:YES];
    // For Android, there no need to do this.
    //
    
    // Create touch listener and register it with cocos2d to receive touch events
    EventListenerTouchOneByOne* touchDelegate = EventListenerTouchOneByOne::create();
    touchDelegate->onTouchBegan = std::bind(&HelloWorld::TouchBegan, this, std::placeholders::_1, std::placeholders::_2);
    touchDelegate->onTouchMoved = std::bind(&HelloWorld::TouchMoved, this, std::placeholders::_1, std::placeholders::_2);
    touchDelegate->onTouchCancelled = std::bind(&HelloWorld::TouchCancelled, this, std::placeholders::_1, std::placeholders::_2);
    touchDelegate->onTouchEnded = std::bind(&HelloWorld::TouchEnded, this, std::placeholders::_1, std::placeholders::_2);
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(touchDelegate, 100);
}

bool HelloWorld::onGestureTap(TGestureTap* gesture)
{
    if (NodeContainsPoint(_sprite, gesture->getLocation()))
    {
        if (gesture->getNumClicks() == 1)
            setInfoLabel("Tap detected");
        else
            setInfoLabel("Double Tap detected");
    }
    
    return false;
}

bool HelloWorld::onGestureLongPress(TGestureLongPress* gesture)
{
    if (NodeContainsPoint(_sprite, gesture->getLocation()))
    {
        setInfoLabel("Long Press detected");
    }
    
    return false;
}

bool HelloWorld::onGesturePan(TGesturePan* gesture)
{
    static int lastPanId = -1;
    static bool panInsideNode = false;
    
    // A new pan
    if (gesture->getID() != lastPanId)
    {
        lastPanId = gesture->getID();
        panInsideNode = NodeContainsPoint(_sprite, gesture->getLocation());
    }
    
    if (panInsideNode)
    {
        _sprite->setPosition(_sprite->getPosition() + gesture->getTranslation());
    }
    
    return false;
}

bool HelloWorld::onGesturePinch(TGesturePinch* gesture)
{
    static int lastPinchId = -1;
    static bool pinchInsideNode = false;
    static float originalScale;
    
    // A new pinch
    if (gesture->getID() != lastPinchId)
    {
        lastPinchId = gesture->getID();
        pinchInsideNode = NodeContainsPoint(_sprite, gesture->getLocation());
        originalScale = _sprite->getScale();
    }
    
    if (pinchInsideNode)
    {
        _sprite->setScale(originalScale * gesture->getScale());
    }
    
    return false;
}

bool HelloWorld::onGestureRotation(TGestureRotation* gesture)
{
    static int lastRotationId = -1;
    static bool rotationInsideNode = false;
    static float originalRotation;
    
    // A new rotation
    if (gesture->getID() != lastRotationId)
    {
        lastRotationId = gesture->getID();
        rotationInsideNode = NodeContainsPoint(_sprite, gesture->getLocation());
        originalRotation = _sprite->getRotation();
    }
    
    if (rotationInsideNode)
    {
        _sprite->setRotation(originalRotation + gesture->getRotation());
    }
    
    return false;
}

bool HelloWorld::onGestureSwipe(TGestureSwipe* gesture)
{
    SwipeDirection dir = gesture->getDirection();
    if (dir == SwipeDirectionNorth)
    {
        setInfoLabel("Swipe detected: NORTH");
    }
    else if (dir == SwipeDirectionNorthEast)
    {
        setInfoLabel("Swipe detected: NORTH EAST");
    }
    else if (dir == SwipeDirectionEast)
    {
        setInfoLabel("Swipe detected: EAST");
    }
    else if (dir == SwipeDirectionSouthEast)
    {
        setInfoLabel("Swipe detected: SOUTH EAST");
    }
    else if (dir == SwipeDirectionSouth)
    {
        setInfoLabel("Swipe detected: SOUTH");
    }
    else if (dir == SwipeDirectionSouthWest)
    {
        setInfoLabel("Swipe detected: SOUTH WEST");
    }
    else if (dir == SwipeDirectionWest)
    {
        setInfoLabel("Swipe detected: WEST");
    }
    else if (dir == SwipeDirectionNorthWest)
    {
        setInfoLabel("Swipe detected: NORTH WEST");
    }
    
    bool panEnabled = true; // If pan is not enabled, use false
    bool swipeFromNode = NodeContainsPoint(_sprite, panEnabled ? gesture->getEndLocation() : gesture->getLocation());
    
    if (swipeFromNode)
    {
        float swipeLength = gesture->getLength();
        float moveDistance = swipeLength * 2;
        
        // Move the sprite along the swipe direction
        Vec2 targetPos = _sprite->getPosition() + (gesture->getDirectionVec().getNormalized() * moveDistance);
        
        MoveTo* move = MoveTo::create(1.5f, targetPos);
        EaseOut* easeout = EaseOut::create(move, 5);
        
        _sprite->runAction(easeout);
    }
    
    return false;
}

bool HelloWorld::TouchBegan(Touch* touch, Event* event)
{
    // Let the gesture recognizer to do its work
    _gestureRecognizer->onTouchBegan(touch, event);
    return true;
}

void HelloWorld::TouchMoved(Touch* touch, Event* event)
{
    // Let the gesture recognizer to do its work
    _gestureRecognizer->onTouchMoved(touch, event);
}

void HelloWorld::TouchEnded(Touch* touch, Event* event)
{
    // Let the gesture recognizer to do its work
    _gestureRecognizer->onTouchEnded(touch, event);
}

void HelloWorld::TouchCancelled(Touch* touch, Event* event)
{
    // Let the gesture recognizer to do its work
    _gestureRecognizer->onTouchCancelled(touch, event);
}

void HelloWorld::setInfoLabel(const std::string& text)
{
    _label->setString(text);
}
